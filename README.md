# 目录说明
```
├───app     程序代码
│   ├───data    应用集
│   ├───getData 数据采集
│   ├───segment ICTCLAS分词处理
│   ├───pre     数据预处理
│   └───topic_detection 话题检测
├───test 测试代码
│   ├───segment 分词处理测试
│   ├───pre     预处理测试
│   └───topic_detection 话题检测测试
├───README.md 说明文档
└───package.json 包管理说明
```

# update

1. 规范化关键信息提示
2016年 2月 3日 星期三 19时35分16秒 CST

# Todos
1. 接口获得数据后直接更新到数据库，不再更新到容器C中统一更新 「时效性」

# 问题
1. 容器C中的数据并未完全更新到数据库中「数据丢失」


# 实验开发

## 实验条件
1. 使用1000条数据走一下流程
2. 过滤规则
    ```
    需要设置标志source 和 address 字段并没有使用到

    a. 删掉status_count在N以上的微博
    b. 删掉字符串中不符合要求的微博
        ##
        【】
    c. 去掉微博所有非中文、非数字字符
        链接
        表情符

    pre/index.js粘合代码，这个函数的输入是什么？
        config

    过滤规则优化
        先暂时不考虑过滤过则的问题，考虑也没有说明意思，先走一遍实验流程
        过滤规则最后可以结合实际情况进行修改

        when_where_what判断条件需要优化一下
        remove_unexpected_string中的规则需要优化一下
    ```
3. 分词
    ```
    分词已经可以使用了
    Data目录使用绝对路径，不同位置文件的文件使用分词系统时，ictclas.cc中的相对地址不能找到Data目录
        修改ictclas.cc文件中 NLPIR_Init("/home/liz/Gitlab/weiboTest/app/segment/ICTCLAS", 1); 使用绝对路径查找Data目录

    分词模块中的性能问题
        不是内存的为题
        单独测试各个模块
            分词模块 ok
            filter weibo
                getNWeibo   ok
                deleteMoreThanNStatusCount ok
                deleteUnexpectedString ok
                removeUnexpectedString not
                    split转数组性能损耗太严重，转用正则表达式处理
    正则表达式
        涉及到大量字符串处理问题，再把正则表达式的内容看看 ok

    分词系统中添加自定义词
        微心情

    对分词结果进行过滤
        a. 天气预报
            %空气%质量%周一%'
            %空气%质量%优良率%'
            delete FROM weiboTest.segmentResult where text like '%质量%空气%';
            delete FROM weiboTest.segmentResult where text like '%空气%排行%';
            delete FROM weiboTest.segmentResult where text like '%空气%质量%';
            delete FROM weiboTest.segmentResult where text like '%空气%霾%';
    ```

4. 算法
    ```
    用户影响力计算
    词语权重计算
        只计算分词结果中的权重 例如"球/n/2#辽宁/ns/2#感情/n/1"
    单个微博微博特征集合
    时间窗口内词语的权重
    ```


# Mysql
1. 查看数据库大小
```
SELECT table_schema "weiboTest",
    sum( data_length + index_length ) / 1024 / 1024 "Data Base Size in MB",
    sum( data_free )/ 1024 / 1024 "Free Space in MB"
FROM information_schema.TABLES
GROUP BY table_schema ;
```
#MongoDB
    ```
    1. 查询总数
        自带的查询语句很强大
        总数
            db.collection.count()
                db.singletermonetimewindowns.count({ 'wids': { $size: 10 }) ok
                db.singletermonetimewindowns.count({ $where: "this.wids.length > 1000" }) ok
                db.singletermonetimewindowns.findOne({ $where: "this.wids.length > 1000" }, {term:1, _id:0}) ok
                db.singletermonetimewindowns.count({ $where: "this.singleTermTotalWeight > 5" }) ok
                db.singletermonetimewindowns.count() ok

                db.burstyterms.count()
                db.twoterms.count({ $where: "this.commonCounts > 0" })
                db.twoterms.findOne({ $where: "this.commonCounts > 1190" }, {twoTerm:1, _id:0})
                db.termweights.count()
        条件查询
            db.collection.count(query)
                db.singletermonetimewindowns.count({singleTermTotalWeight : { $gt: 10})

                db.termweights.count({flag : true})

                db.singletermonetimewindowns.count( {singleTermTotalWeight: { $gt: 1 } })
        查看collection大小
            db.singletermonetimewindowns.dataSize() 返回结果为字节
        更新
            db.burstyterms.update({'flag' : true}, {'flag' : false})
    ```
# key value数据库
    ```
    Redis

    MongoDB
        字符串可以用buffer存储 试试
    学学，mongodb
    ```


# 开发
1. 本地和服务器内容可能不同步，先同步下
2. 异步的代码写起来确实麻烦点，看看ES6是如何解决的
3. 采集到的数据可以直接进行过滤分词，然后存储
    前提是最终的过滤、分词规则已经确定完毕
4. 表中时间应该存成时间戳
    挺重要的
        暂时 将分词结果中的时间转换为时间戳
        等一切确定有再将采集中的时间存储为时间戳
5. mocha 处理异步的时候，should.equal失效
    在使用返回的promise对象时，出现错误
6. 微博中存储的时间就是当地时间
7. 慎用setInterval，它不主动释放内存，注意手动释放
8. 获取突发词集的时候没有考虑 用户影响力

# 节奏
    白天实验 晚上报告

论文创新点
1. 没有现成可利用数据集，自己采集
2. 添加地理位置信息
    一二章介绍的时候多提 移动互联网
3. 优化突发词计算流程
4. 优化聚类算法
    非关系型数据库
5. 过滤
    分词前多虑
    分词后也过滤
6. 只保留三类关键字


node --max-old-space-size=800 app/topic_detection/burst_word_detection/index.js
node --max-old-space-size=1024 app/topic_detection/cluster/index.js

突发词聚类

```
1. 计算突发词涉及微博总数
    先假设总数为 37000
    准确值 361097
2. 仅仅取出所有词语
    ok
3. redis查询
    导入
    查询
        如何存
        如何用
    试一下全部加载到内存中的情况
        不要redis不要异步
            ES6如何解决异步的，一定要看看
                太蛋疼了

4. 聚类 ok
```
