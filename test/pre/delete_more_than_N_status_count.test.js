/**
 * 删除statusCount总数大于N的微博
 * @param  {Array} arr getNWeibo中返回的结果
 * @param  {Number} N   搜集status_count大于N的wid
 * @return {Array}     数组，元素为wid
 */
const mockWeiboTestData = require('../../app/data/mock_weibo_test_data');

const deleteMoreThanNStatusCount = require('../../app/pre/delete_more_than_N_status_count');

describe('pre 预处理', function() {
    describe('deleteMoreThanNStatusCount 删除输入数组中StatusCount大于N的微博', function() {

        it('返回结果类型为数组', function() {
            var input = mockWeiboTestData();
            deleteMoreThanNStatusCount(input, 1000).should.Array();
        });

        it('返回结果与输入数据形式上保持一致', function() {
            var input = mockWeiboTestData();
            var i = 0,
                j = 0;
            for (var key in input[0]) {
                if (key) {
                    i++;
                }
            }
            for (var keyJ in deleteMoreThanNStatusCount(input, 1000)[0]) {
                if (keyJ) {
                    j++;
                }
            }
            (i).should.equal(j);
        });

        it('返回结果中statuses_count小于N', function() {
            var input = mockWeiboTestData();
            deleteMoreThanNStatusCount(input, 1000).length.should.equal(3);
            deleteMoreThanNStatusCount(input, 100).length.should.equal(2);
            deleteMoreThanNStatusCount(input, 10).length.should.equal(1);
        });
    });
});
