const deleteUnexpectedString = require('../../app/pre/delete_unexpected_string');

describe('pre 预处理', function() {
    describe('deleteUnexpectedString 删除包含某些特征字符串的微博', function() {

        var mockWeiboTestData = require('../../app/data/mock_weibo_test_data');

        it('返回结果为数组', function() {
            var data = mockWeiboTestData();
            (deleteUnexpectedString(data)).should.Array();
        });
        it('剔除包含“#主题#、【主题】”格式的微博', function() {
            var data = mockWeiboTestData();
            deleteUnexpectedString(data).length.should.equal(2);
        });
    });

});
