const removeUnexpectedString = require('../../app/pre/remove_unexpected_string');

describe('pre 预处理', function() {
    describe('removeUnexpectedString 去除微博中链接、表情符', function() {
        var input = [{
            wid: 1,
            text: '今天真是开心[开心]'
        }, {
            wid: 2,
            text: '[开心]今天真是开心[开心]'
        }, {
            wid: 3,
            text: '[开心]2016年3月18号，今天真是开心[开心]http://baidu.com是个技术很不错的公司'
        }];
        it('去除表情符[开心]等', function() {
            var result = [{
                wid: 1,
                text: '今天真是开心'
            }, {
                wid: 2,
                text: '今天真是开心'
            }, {
                wid: 3,
                text: '2016年3月18号今天真是开心是个技术很不错的公司'
            }];
            removeUnexpectedString(input).should.eql(result);
        });
        it('去除非中文、非数字字符', function() {
            var result = [{
                wid: 1,
                text: '今天真是开心'
            }, {
                wid: 2,
                text: '今天真是开心'
            }, {
                wid: 3,
                text: '2016年3月18号今天真是开心是个技术很不错的公司'
            }];
            removeUnexpectedString(input).should.eql(result);
        });
    });
});
