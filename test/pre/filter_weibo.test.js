var filterWeibo = require('../../app/pre/filter_weibo.js');
var deleteMoreThanNStatusCount = require('../../app/pre/delete_more_than_N_status_count');
var deleteUnexpectedString = require('../../app/pre/delete_unexpected_string');
var getNWeibo = require('../../app/pre/get_N_weibo');
var removeUnexpectedString = require('../../app/pre/remove_unexpected_string');
var mockWeiboTestData = require('../../app/data/mock_weibo_test_data');

describe('filter weibo 过滤微博', function() {
    describe('deleteMoreThanNStatusCount 删除输入数组中StatusCount大于N的微博', function() {

        it('返回结果类型为数组', function() {
            var input = mockWeiboTestData();
            deleteMoreThanNStatusCount(input, 1000).should.Array();
        });

        it('返回结果与输入数据形式上保持一致', function() {
            var input = mockWeiboTestData();
            var i = 0,
                j = 0;
            for (var key in input[0]) {
                if (key) {
                    i++;
                }
            }
            for (var keyJ in deleteMoreThanNStatusCount(input, 1000)[0]) {
                if (keyJ) {
                    j++;
                }
            }
            (i).should.equal(j);
        });

        it('返回结果中statuses_count小于N', function() {
            var input = mockWeiboTestData();
            deleteMoreThanNStatusCount(input, 1000).length.should.equal(3);
            deleteMoreThanNStatusCount(input, 100).length.should.equal(2);
            deleteMoreThanNStatusCount(input, 10).length.should.equal(1);
        });
    });
    describe('deleteUnexpectedString 删除包含某些特征字符串的微博', function() {

        var mockWeiboTestData = require('../../app/data/mock_weibo_test_data');

        it('返回结果为数组', function() {
            var data = mockWeiboTestData();
            (deleteUnexpectedString(data)).should.Array();
        });

        it('剔除包含“#主题#、【主题】”格式的微博', function() {
            var data = mockWeiboTestData();
            deleteUnexpectedString(data).length.should.equal(2);
        });
    });

    describe('getNWeibo 获得N个微博', function() {
        this.timeout(20000);
        it('返回指定N个数据，返回结果为数组', function(done) {
            getNWeibo(4).then(function(result) {
                result.length.should.equal(4);
                done();
            })
        });

        it('返回数组中的元素为对象', function(done) {
            getNWeibo(2).then(function(result) {
                result[0].should.Object();
                done();
            })
        });

        it('返回数组中的对象属性与数据库中列属性一一对应', function(done) {
            getNWeibo(2).then(function(result) {
                (typeof result[0].text).should.equal('string');
                (typeof result[0].wid).should.equal('number');
                (typeof result[0].geo).should.equal('string');
                done();
            })
        });

        it('当已取出数据库中所有数据时，返回空数组');
    });

    describe('removeUnexpectedString 去除微博中链接、表情符', function() {
        var input = [{
            wid: 1,
            text: '今天真是开心[开心]'
        }, {
            wid: 2,
            text: '[开心]今天真是开心[开心]'
        }, {
            wid: 3,
            text: '[开心]2016年3月18号，今天真是开心[开心]http://baidu.com是个技术很不错的公司'
        }];

        it('去除表情符[开心]等', function() {
            var result = [{
                wid: 1,
                text: '今天真是开心'
            }, {
                wid: 2,
                text: '今天真是开心'
            }, {
                wid: 3,
                text: '2016年3月18号今天真是开心是个技术很不错的公司'
            }];
            removeUnexpectedString(input).should.eql(result);
        });

        it('去除非中文、非数字字符', function() {
            var result = [{
                wid: 1,
                text: '今天真是开心'
            }, {
                wid: 2,
                text: '今天真是开心'
            }, {
                wid: 3,
                text: '2016年3月18号今天真是开心是个技术很不错的公司'
            }];
            removeUnexpectedString(input).should.eql(result);
        });
    });
    describe('filterWeibo 代码粘合', function() {

        it('filterWeibo 将数据预处理的所有过程粘合起来', function(done) {
            filterWeibo({
                N: 5,
                statusCountLimit: 1000
            }).then(function(arr) {
                // console.log(JSON.stringify(arr, null, 4));
                done();
            });
        });
    });
});
