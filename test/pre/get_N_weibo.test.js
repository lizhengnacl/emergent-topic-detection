/**
 * 从数据库中获取N条微博
 * 输入：需要返回的数据个数N
 * 输出: 包含N条微博的数组
 */
const getNWeibo = require('../../app/pre/get_N_weibo');

describe('pre 预处理', function() {
    describe('getNWeibo 获得N个微博', function() {
        this.timeout(20000);
        it('返回指定N个数据，返回结果为数组', function(done) {
            getNWeibo(4).then(function(result) {
                result.length.should.equal(4);
                done();
            })
        });
        it('返回数组中的元素为对象', function(done) {
            getNWeibo(2).then(function(result) {
                result[0].should.Object();
                done();
            })
        });
        it('返回数组中的对象属性与数据库中列属性一一对应', function(done) {
            getNWeibo(2).then(function(result) {
                (typeof result[0].text).should.equal('string');
                (typeof result[0].wid).should.equal('number');
                (typeof result[0].geo).should.equal('string');
                done();
            })
        });
        it('当已取出数据库中所有数据时，返回空数组', function() {

        });
    });
});
