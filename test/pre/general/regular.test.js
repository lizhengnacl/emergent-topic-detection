var regular = require('../../../app/pre/general/regular.js');

describe('pre', function() {
    describe('general', function() {
        describe('#emoticons 匹配格式为“[大笑]”的表情符', function() {
            it('[li眼泪ｎｉａ]zheng[fvck]na[大笑]c[泪]l', function() {
                var reg = regular.emoticons;
                var input = '[li眼泪ｎｉａ]zheng[fvck]na[大笑]c[泪]l';
                var result = 'zhengnacl';
                input.replace(reg, '').should.eql(result);
            });
        });
        describe('#nonChineseCharacter 匹配非中文、非数字字符', function() {
            it('hello world 你好 ｈａｈａ　123', function() {
                var reg = regular.nonChineseCharacter;
                var input = 'hello world 你好 ｈａｈａ　123'
                var result = '你好123'
                input.replace(reg, '').should.eql(result);
            });
        });
    });
});
