const deleteSpecifiedConditions = require('../../../app/pre/general/delete_specified_conditions.js');

const mockWeiboTestData = require('../../../app/data/mock_weibo_test_data');

describe('pre', function() {
    describe('general', function() {
        describe('deleteSpecifiedConditions 删除数组A中在数组B中出现的元素', function() {
            it('数组B个数为0时', function() {
                var arr = mockWeiboTestData();
                deleteSpecifiedConditions(arr, []).should.equal(arr);
            });
            it('数组B中包含数组A中所有的元素时', function() {
                var arr = mockWeiboTestData();
                var condition = [];
                for (var i = 0; i < arr.length; i++) {
                    condition.push(arr[i].wid);
                };
                var result = deleteSpecifiedConditions(arr, condition);
                result.length.should.equal(0);
                result.should.Array();
            });
        });
    });
});
