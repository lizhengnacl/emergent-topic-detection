var find = require('../../../../app/topic_detection/burst_word_detection/mongodb/find_single_term_weight_within_one_time_window.js');

describe('topic detection 话题检测模块', function() {
    describe('burst word detection 突发词检测', function() {
        describe('mongodb', function() {
            it('find', function(done) {
                var input = {
                    term: 'xiaoming'
                };
                find(input).then(function(result) {
                    try {
                        console.log(JSON.stringify(result, null, 4));
                        // 找到返回
                        // {
                        //     "_id": "56f27b9e249184f505d48eff",
                        //     "term": "liz test",
                        //     "__v": 0,
                        //     "wids": []
                        // }
                        // 未找到返回null，不是'null'
                    } catch (e) {
                        console.log(e);
                    }
                    done();
                });
            });
        });
    });
});
