var create = require('../../../../app/topic_detection/burst_word_detection/mongodb/create_single_term_weight_within_one_time_window.js');

describe('topic detection 话题检测模块', function() {
    describe('burst word detection 突发词检测', function() {
        describe('mongodb', function() {
            it('find', function(done) {
                var input = {
                    term: '小明',
                    wids: [{
                        wid: 1,
                        userInfluence: 10
                    }],
                    singleTermTotalWeight: 1
                };
                create(input).then(function(result) {
                    console.log(JSON.stringify(result, null, 4));
                    // 只有在myScheme中出现改属性，修改才生效
                    // result.term = 'liz test';
                    // result.save();
                    done();
                });
            });
        });
    });
});
