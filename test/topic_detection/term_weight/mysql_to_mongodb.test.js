var mysqlToMongoDB = require('../../../app/topic_detection/term_weight/mysql_to_mongodb.js');

describe('topic detection 话题检测模块', function() {
    describe('term weight 单个微博中词语权重', function() {
        describe('mysql to mongodb', function() {
            it('', function() {
                var input = {
                    "id": 771,
                    "created_at": "Sat Mar 19 22:13:55 +0800 2016",
                    "wid": 3954865334091383,
                    "text": "心态/n/1#老师/n/1#北京/ns/2",
                    "source": 1,
                    "reposts_count": 0,
                    "comments_count": 0,
                    "attitudes_count": 0,
                    "geo": "null",
                    "province_name": "",
                    "city_name": "北京",
                    "district_name": "",
                    "address": "",
                    "uid": 3027726535,
                    "location": "山西 太原",
                    "followers_count": 20,
                    "friends_count": 98,
                    "statuses_count": 86,
                    "verified": 0
                }
                var result = {
                    createAt: 1458396835000,
                    flag: false,
                    maxFrequency: 2,
                    terms: [{
                        frequency: 1,
                        term: '心态',
                        weight: 0.9
                    }, {
                        frequency: 1,
                        term: '老师',
                        weight: 0.9
                    }, {
                        frequency: 2,
                        term: '北京',
                        weight: 1.2
                    }],
                    wid: 3954865334091383,
                    userInfluence: 980,
                    propagationWeight: 0
                }
                result.terms.maxFrequency = 2;
                mysqlToMongoDB(input).should.eql(result);
            });
        });
    });
});
