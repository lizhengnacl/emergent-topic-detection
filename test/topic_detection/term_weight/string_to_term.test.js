var stringToTerm = require('../../../app/topic_detection/term_weight/string_to_terms.js');

describe('topic detection 话题检测模块', function() {
    describe('term weight 单个微博中词语权重', function() {
        describe('string to term 处理分词结果字符串', function() {
            it('返回结果为数组', function() {
                var input = '球/n/2#辽宁/ns/2#感情/n/1';
                stringToTerm(input).should.Array();
            });
            it('返回数组中元素为对象', function() {
                var input = '球/n/2#辽宁/ns/2#感情/n/1';
                stringToTerm(input)[0].should.Object();
            });
            it('返回结果中包含maxFrequency属性', function() {
                var input = '球/n/2#辽宁/ns/2#感情/n/1';
                stringToTerm(input).maxFrequency.should.equal(2);
            });
            it('返回结果形式为', function() {
                var input = '球/n/2#辽宁/ns/2#感情/n/1';
                var result = [{term: '球', frequency : 2},{term: '辽宁', frequency : 2},{term: '感情', frequency : 1}];
                result.maxFrequency = 2;
                stringToTerm(input).should.eql(result);
            });
        });
    });
});