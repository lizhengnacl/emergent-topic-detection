var storeTerms = require('../../../app/topic_detection/term_weight/store_terms.js');

describe('topic detection 话题检测模块', function() {
    describe('term weight 单个微博中词语权重', function() {
        describe('store terms 将输入的对象格式存储为document', function() {
            it('非严格模式，可以存储Schema之外的值', function(done) {
                var singleWeibo = {};
                singleWeibo.wid = 3;
                singleWeibo.terms = [];
                var obj = {
                    term: '哈尔滨/ns/',
                    frequency: 4
                };
                singleWeibo.terms.push(obj);
                singleWeibo.terms.push(obj);
                singleWeibo.test = 'strict test';
                storeTerms(singleWeibo).then(function(msg) {
                    msg.should.equal('save success');
                    done();
                });
            });
        });
    });
});
