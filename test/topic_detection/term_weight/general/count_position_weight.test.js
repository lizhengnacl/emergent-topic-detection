var countPositionWeight = require('../../../../app/topic_detection/term_weight/general/count_position_weight.js');

describe('topic detection 话题检测模块', function() {
    describe('term weight 单个微博中词语权重', function() {
        describe('general', function() {
            describe('countPositionWeight 计算位置权重', function() {
                it('', function() {
                    var input = {
                        text: '球/n/2#辽宁/ns/2#感情/n/1',
                        district_name: '',
                        city_name: '辽宁',
                        province_name: ''
                    };
                    countPositionWeight(input).should.equal(1.2);
                });
            });
        });
    });
});
