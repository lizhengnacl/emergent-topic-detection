var countUserInfluence = require('../../../../app/topic_detection/term_weight/general/count_user_influence.js');

describe('topic detection 话题检测模块', function() {
    describe('term weight 单个微博中词语权重', function() {
        describe('general', function() {
            describe('countUserInfluence 计算用户影响力', function() {
                it('', function() {
                    var input = {
                        "id": 771,
                        "created_at": "Sat Mar 19 22:13:55 +0800 2016",
                        "wid": 3954865334091383,
                        "text": "心态/n/1#老师/n/1#北京/ns/2",
                        "source": 1,
                        "reposts_count": 2,
                        "comments_count": 2,
                        "attitudes_count": 2,
                        "geo": "null",
                        "province_name": "",
                        "city_name": "北京",
                        "district_name": "",
                        "address": "",
                        "uid": 3027726535,
                        "location": "山西 太原",
                        "followers_count": 171,
                        "friends_count": 244,
                        "statuses_count": 215,
                        "verified": 0
                    }
                    countUserInfluence(input).should.equal(20862);
                });
            });
        });
    });
});
