var countPropagationWeight = require('../../../../app/topic_detection/term_weight/general/count_propagation_weight.js');

describe('topic detection 话题检测模块', function() {
    describe('term weight 单个微博中词语权重', function() {
        describe('general', function() {
            describe('countPropagationWeight 计算传播特性权重', function() {
                it('', function() {
                    var input = {
                        "id": 771,
                        "created_at": "Sat Mar 19 22:13:55 +0800 2016",
                        "wid": 3954865334091383,
                        "text": "心态/n/1#老师/n/1#北京/ns/2",
                        "source": 1,
                        "reposts_count": 2,
                        "comments_count": 2,
                        "attitudes_count": 2,
                        "geo": "null",
                        "province_name": "",
                        "city_name": "北京",
                        "district_name": "",
                        "address": "",
                        "uid": 3027726535,
                        "location": "山西 太原",
                        "followers_count": 20,
                        "friends_count": 98,
                        "statuses_count": 86,
                        "verified": 0
                    }
                    countPropagationWeight(input).should.equal(12);
                });
            });
        });
    });
});
