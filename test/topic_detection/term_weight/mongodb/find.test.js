var find = require('../../../../app/topic_detection/term_weight/mongodb/find.js');

describe('topic detection 话题检测模块', function() {
    describe('term weight 单个微博中词语权重', function() {
        describe('mongodb', function() {
            it('find', function(done) {
                var input = {
                    flag: true
                };
                find(input).then(function(result) {
                    result.flag = false;
                    result.save();
                    // console.log(JSON.stringify(result, null, 4));
                    done();
                });
            });
        });
    });
});
