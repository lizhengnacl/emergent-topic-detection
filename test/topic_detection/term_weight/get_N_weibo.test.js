var getNWeibo = require('../../../app/topic_detection/term_weight/get_N_weibo.js');

describe('topic detection 话题检测模块', function() {
    describe('term weight 单个微博中词语权重', function() {
        describe('get N weibo 从segmentResult中获取N个微博', function() {
            it('返回指定N个结果', function(done) {
                getNWeibo(4).then(function(arr) {
                    try {
                        arr.length.should.equal(4);
                    } catch (e) {
                        console.log(e);
                    }
                    done();
                });
            });
        });
    });
});
