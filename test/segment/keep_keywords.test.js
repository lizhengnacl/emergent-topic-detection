var keepKeywords = require('../../app/segment/keep_keywords.js');

describe('segment', function() {
    describe('keepKeywords', function() {
        it('只保留时间、地点、事件三类关键则', function() {
            var input = [{
                wid: 1,
                text: '今天/t/1#哈尔滨/ns/1#天气/n/1#不错/a/1'
            }, {
                wid: 2,
                text: '今天/t/1#今天/t/1#今天/t/1#哈尔滨/ns/1#天气/n/1#不错/a/1'
            }, {
                wid: 3,
                text: '今天/t/1#哈尔滨/ns/1#哈尔滨/ns/1#哈尔滨/ns/1#天气/n/1#不错/a/1'
            }];

            var result = [{
                wid: 1,
                text: '今天/t/1#哈尔滨/ns/1#天气/n/1'
            }, {
                wid: 2,
                text: '今天/t/1#今天/t/1#今天/t/1#哈尔滨/ns/1#天气/n/1'
            }, {
                wid: 3,
                text: '今天/t/1#哈尔滨/ns/1#哈尔滨/ns/1#哈尔滨/ns/1#天气/n/1'
            }];

            keepKeywords(input).should.eql(result);
        });
    });
});
