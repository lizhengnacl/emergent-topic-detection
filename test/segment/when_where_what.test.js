var whenWhereWhat = require('../../app/segment/when_where_what.js');

describe('segment', function() {
    describe('whenWhereWhat', function() {
        it('何时何地何事 三要素缺一不可', function() {
            var input = [{
                wid: 1,
                text: '今天/t/1#哈尔滨/ns/1#天气/n/1#不错/a/1#'
            }, {
                wid: 2,
                text: '哈尔滨/ns/1#天气/n/1#不错/a/1#'
            }, {
                wid: 3,
                text: '今天/t/1#天气/n/1#不错/a/1#'
            }, {
                wid: 4,
                text: '今天/t/1#哈尔滨/ns/1#不错/a/1#'
            }];

            var result = [{
                wid: 1,
                text: '今天/t/1#哈尔滨/ns/1#天气/n/1#不错/a/1#'
            }];
            whenWhereWhat(input).should.eql(result);
        });
    });
});
