var segment = require('../../app/segment/segment.js');

describe('segment 分词处理', function() {
    describe('segment 分词系统词频统计接口', function() {
        it('返回结果为字符串', function() {
            segment('哈尔滨').should.String();
        });
        it('返回字符串格式 "今天/t/1#哈尔滨/ns/1#天气/n/1#不错/a/1#"', function() {
            this.timeout(100000);
            var str = segment('今天哈尔滨天气不错');
            // var count = 0;
            // for (var i = 0; i < 100000; i++) {
            //     segment('今天哈尔滨天气不错');
            //     count++;
            // };
            // console.log(count);
            // var str = segment('2016年3月19日哈尔滨天气不错');
            // console.log(str);
            // str 等于 '今天/t/1#哈尔滨/ns/1#天气/n/1#不错/a/1#'
            // str.split('#') 等于 [ '今天/t/1', '哈尔滨/ns/1', '天气/n/1', '不错/a/1', '' ]
            str.split('#').length.should.eql(5);
        });
    });
});
