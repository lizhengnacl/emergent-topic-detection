var ictclas = require('./ICTCLAS/index.js');
/**
 * 调用ICTCLAS分词系统，自动完成初始化
 * @return {[@function]} 词频统计接口，该接口接受一个参数
 */
module.exports = ictclas.wordFreqStat;
