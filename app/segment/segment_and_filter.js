var whenWhereWhat = require('./when_where_what.js');
var wordFreqStat = require('./word_frequency_statistics.js');
var keepKeywords = require('./keep_keywords.js');
/**
 * 对微博内容进行分词处理并过滤掉没有三要素“何时何地何事”的分词结果
 * @param  {Array} arr 微博数组
 * @return {Array}     处理后的微博数组
 */
module.exports = function(arr) {
    wordFreqStat(arr);
    whenWhereWhat(arr);
    keepKeywords(arr);
    return arr;
};
