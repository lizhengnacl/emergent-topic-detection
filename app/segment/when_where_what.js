var deleteSpecifiedConditions = require('../pre/general/delete_specified_conditions.js');

module.exports = function(arr) {
    var deleteArr = [];
    for (var i = 0; i < arr.length; i++) {
        var str = arr[i].text;
        if (str.indexOf('/ns/') === -1) {
            deleteArr.push(arr[i].wid);
            continue;
        }
        if (str.indexOf('/t/') === -1) {
            deleteArr.push(arr[i].wid);
            continue;
        }
        if (str.indexOf('/n/') === -1) {
            deleteArr.push(arr[i].wid);
            continue;
        }
    };
    return deleteSpecifiedConditions(arr, deleteArr);
};
