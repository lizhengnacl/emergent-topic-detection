var wordFreqStat = require('./ICTCLAS/index.js').wordFreqStat;

/**
 * 对微博数组中的微博内容进行分词
 * @param  {Array} arr 微博数组
 * @return {Array}     微博数组
 */
module.exports = function(arr) {
    for (var i = 0; i < arr.length; i++) {
        var str = wordFreqStat(arr[i].text);
        // 去掉分词结果字符串中末尾的#号，后续再使用str.split('#')的
        // 结果数组中末尾就不会出现空字符串了
        str = str.substr(0, str.length - 1);
        arr[i].text = str;
    };
    return arr;
};
