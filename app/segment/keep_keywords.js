/**
 * 保留微博中表示时间/t、地点/ns、事件/n的三类名词
 * @param  {Array} arr 微博数组，text格式为'今天/t/1#哈尔滨/ns/1#天气/n/1#不错/a/1'
 * @return {Array}     微博数组，text格式为'今天/t/1#哈尔滨/ns/1#天气/n/1'
 */
module.exports = function(arr) {
    for (var i = 0; i < arr.length; i++) {
        var strArr = arr[i].text.split('#');
        var keywordsArr = [];
        for (var j = 0; j < strArr.length; j++) {
            if (judgeKeepKeywords(strArr[j])) {
                keywordsArr.push(strArr[j]);
            }
        };
        arr[i].text = keywordsArr.join('#');
    };

    function judgeKeepKeywords(str) {
        if (str.indexOf('/t/') !== -1) {
            return true;
        }
        if (str.indexOf('/ns/') !== -1) {
            return true;
        }
        if (str.indexOf('/n/') !== -1) {
            return true;
        }
        return false;
    }
    return arr;
};
