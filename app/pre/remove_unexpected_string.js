/**
 * 去除微博中的表情符，去除微博中所有非中文、非数字字符
 * @param  {[Array]} arr 微博数组
 * @return {Array}     处理后的微博数组
 */
var regular = require('./general/regular.js');

module.exports = function(arr) {
    var reg = /[^\u4E00-\u9FA5\uF900-\uFA2D0-9]/g;
    for (var i = 0; i < arr.length; i++) {
        // 去掉表情符[]
        arr[i].text = arr[i].text.replace(regular.emoticons, '');
        // 去掉非中文字符
        arr[i].text = arr[i].text.replace(regular.nonChineseCharacter, '');
    };
    // console.log('remove_unexpected_string arr.length : ' + arr.length);
    return arr;
};
