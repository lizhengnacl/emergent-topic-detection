/**
 * 从数据库表A中获取N条微博，用source字段做标记，将已经取出的记录在表A中做好标记，
 * 输入：需要返回的数据个数N
 * 输出: 包含N条微博的数组，为promise对象
 */
const WeiboTestData = require('../db/weiboTestData');

module.exports = function(N) {
    return new Promise(function(resolve, reject) {
        WeiboTestData
            .findAll({
                where: {
                    source: 0
                },
                limit: N
            })
            .then(function(result) {
                resolve(result);
                // 标记
                for (var i = 0; i < result.length; i++) {
                    result[i].source = 1;
                    result[i].save();
                };
            });
    });
};
