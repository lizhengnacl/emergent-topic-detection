/**
 * 删除包含某些特征字符串的微博
 *     1. #主题#
 *     2. 【主题】
 *     3. @
 *     4. 字符数量太小
 *
 * @type {[type]}
 */
const deleteSpecifiedConditions = require('./general/delete_specified_conditions');

module.exports = function(arr) {
    var deleteArr = [];
    for (var i = 0; i < arr.length; i++) {
        var tmp = arr[i];
        //
        tmp.text = tmp.text.trim();
        // #主题#
        if (tmp.text.indexOf('#') !== -1) {
            var start = tmp.text.indexOf('#');
            var end = tmp.text.lastIndexOf('#');
            if (start !== end) {
                deleteArr.push(tmp.wid);
            }
        }
        // 【主题】
        if (tmp.text.indexOf('【') !== -1) {
            var start = tmp.text.indexOf('【');
            var end = tmp.text.lastIndexOf('】');
            if (start !== end) {
                deleteArr.push(tmp.wid);
            }
        }
        // @
        if (tmp.text.indexOf('@') === 0) {
            deleteArr.push(tmp.wid);
        }
        // 字符数小于10个
        if (tmp.text.length < 10) {
            deleteArr.push(tmp.wid);
        }
    };
    // console.log('delete_unexpected_string deleteArr.length : ' + deleteArr.length);
    return deleteSpecifiedConditions(arr, deleteArr);
};
