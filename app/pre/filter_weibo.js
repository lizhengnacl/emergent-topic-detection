var getNWeibo = require('./get_N_weibo');
var deleteMoreThanNStatusCount = require('./delete_more_than_N_status_count');
var deleteUnexpectedString = require('./delete_unexpected_string');
var removeUnexpectedString = require('./remove_unexpected_string');
/**
 * 去除config.N个微博进行批量预处理
 *
 * @param  {[Object]} config 配置对象
 * @return {Array}        微博数组
 */
module.exports = function(config) {
    var defaultConfig = {
        N: 10,
        statusCountLimit: 1000000
    };
    if (config === undefined) {
        config = defaultConfig;
    } else {
        for (var key in config) {
            defaultConfig[key] = config[key];
        }
    }
    return new Promise(function(resolve, reject) {
        getNWeibo(defaultConfig.N).then(function(arr) {
            // console.log('getNWeibo : ' + arr.length);
            deleteMoreThanNStatusCount(arr, defaultConfig.statusCountLimit);
            deleteUnexpectedString(arr);
            removeUnexpectedString(arr);
            // console.log('filter weibo :' + arr.length);
            resolve(arr);
        })
    });
};
