/**
 * 删掉数组arr中满足condition的元素
 * @param  {Array} arr       包含微博数据对象的数组，形式与../../data/mock_weibo_test_data.js中相同
 * @param  {Array} condition 包含微博wid的数组，例如[wid, wid, wid]
 * @return {Array}           与arr相同
 */
module.exports = function(arr, condition) {
    // 可以用“插入排序进行优化”
    for (var i = 0; i < condition.length; i++) {
        var wid = condition[i];
        for (var j = 0; j < arr.length; j++) {
            var tmpDelete = arr[j];
            if (tmpDelete.wid === wid) {
                arr.splice(j, 1);
                break;
            }
        };
    };
    // console.log('delete_specified_conditions arr.length:' + arr.length);
    return arr;
}
