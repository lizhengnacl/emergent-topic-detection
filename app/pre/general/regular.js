/**
 * 匹配字符串中的表情符，例如[大笑]、[泪]
 * @type {RegExp}
 */
exports.emoticons = /\[[^\]]*\]/g;

/**
 * 匹配非中文字符
 * @type {RegExp}
 */
exports.nonChineseCharacter = /[^\u4E00-\u9FA5\uF900-\uFA2D0-9]/g;
