/**
 * 删除statusCount总数大于1000000的微博
 * @param  {Array} arr getNWeibo中返回的结果
 * @param  {Number} N   搜集status_count大于N的wid
 * @return {Array}     数组
 */
const deleteSpecifiedConditions = require('./general/delete_specified_conditions');

module.exports = function(arr, N) {
    var deleteArr = [];
    for (var i = 0; i < arr.length; i++) {
        var tmp = arr[i];
        if (tmp.statuses_count > N) {
            deleteArr.push(tmp.wid);
        }
    }
    // console.log('delete more than N status count deleteArr.length: ' + deleteArr.length);
    return deleteSpecifiedConditions(arr, deleteArr);
}
