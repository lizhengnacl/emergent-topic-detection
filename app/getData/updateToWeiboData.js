// 将容器C中数据更新到数据库中
var WeiboTestData = require('../db/weiboTestData');

function updateToWeiboTestData(c) {
  return new Promise(function(resolve, reject) {
    var count = 0;
    for (var i = 0, len = c.length; i < len; i++) {
      var wid = c[i].wid
      var data = c[i].data;
      if (data !== undefined) {
        count++;
        if (data.geo) {
          WeiboTestData.update({
            geo: 'null',
          }, {
            where: {
              wid: wid
            }
          });
        }
        if (data.province_name === 'null') {
          WeiboTestData.update({
            province_name: 'null',
          }, {
            where: {
              wid: wid
            }
          });
        }
        if (data.province_name !== '') {
          WeiboTestData.update({
            province_name: data.province_name,
            city_name: data.city_name,
            district_name: data.district_name,
            address: data.address
          }, {
            where: {
              wid: wid
            }
          });
        }
      }
    }
    console.log('#updateToWeiboTestData : 容器C中包含' + (count + 1) + '个新数据');
    resolve();
  });
}

module.exports = updateToWeiboTestData;
