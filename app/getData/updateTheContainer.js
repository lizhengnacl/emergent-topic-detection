var request = require('request');

function updateTheContainer(c, at, countC, accessTokenWillBeRemoved) {
  var url = 'https://api.weibo.com/2/location/geo/geo_to_address.json' + '?access_token=' + at + '&coordinate=' + c[countC.num].coordinate;
  request.get(url, function(err, res, body) {
    if (!err && res.statusCode == 200) {
      body = JSON.parse(body);
      if (body.geos) {
        var data = body.geos[0];
        if (data.province_name !== '') {
          c[countC.num].data = data;
          // record.update({
          //   province_name: data.province_name,
          //   city_name: data.city_name,
          //   district_name: data.district_name,
          //   address: data.address
          // });
        } else if (data.province_name === '') {
          c[countC.num].data = {
              province_name: 'null',
              city_name: '',
              district_name: '',
              address: ''
            }
            // record.update({
            //   // geo接口调用成功，但返回数值为空，则跳过此条微博，并用'null'标示
            //   // 这种情况是存在的，例如：
            //   // 1. 国外
            //   // 2. 微博接口范围经纬度为负值时
            //   province_name: 'null'
            // });
        }
      } else {
        c[countC.num].data = {};
        c[countC.num].data.geo = 'null';
        // record.update({
        //   geo: 'null'
        // });
      }
      countC.num++;
    } else {
      // 接口调用出错，切换access_token
      console.log('#updateTheContainer : not return 200');
      // 这里半天也测不出来
      accessTokenWillBeRemoved.push(at);
      accessTokenWillBeRemoved.removeAccessToken = true;
      // console.log('geo : change to the ' + countArr + ' access_token');
      countC.num++;
    }
  });
}

module.exports = updateTheContainer;
// 具体更新部分得跟第三部分接触后才能确定

// logs
// 44, 45的变量需要再斟酌一下

// 46行 countArr 没有传进来，暂时注销掉
