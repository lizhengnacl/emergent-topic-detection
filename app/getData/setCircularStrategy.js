// 设置接口的调用策略

function setCircularStrategy(c, arr, updateTheContainer, countC) {
  // 设置循环策略
  var countArr = 0;
  var interval = 3000;
  var orderTime = 3 * interval;
  // var removeAccessToken = false;
  // 存储待删除的access_token
  var accessTokenWillBeRemoved = [];
  // 如果一个access_token失效，剔除后应该立马进入下一个interval
  // 放在对象下可按引用传递
  accessTokenWillBeRemoved.removeAccessToken = false;
  var start = new Date().getTime();

  var timer = setInterval(function() {
    // 增加一个判断条件，当容器C中的数据转换完毕时，停止请求数据
    if (arr.length === 0 || countC.num >= c.length - 1) {
      clearInterval(timer);
    }
    if (new Date().getTime() - start > orderTime || accessTokenWillBeRemoved.removeAccessToken) {
      // 如何条件不满足，是不会重置start的！
      start = new Date().getTime();
      accessTokenWillBeRemoved.removeAccessToken = false;
      if (countArr < arr.length - 1) {
        countArr++;
      } else {
        removeFrom(arr, accessTokenWillBeRemoved);
        accessTokenWillBeRemoved = [];
        accessTokenWillBeRemoved.removeAccessToken = false;
        countArr = 0;
      }
      console.log('geo : ' + 'change to the ' + countArr + ' access_token');
    }
    if (arr.length) {
      // Request(arr[countArr]);
      // !!更新容器!!
      // 调用容器C的容量个接口后，interval并未停止，后续还会继续调用接口么？如果是，是一直用的arr中的最后一个么？
      console.log('#setCircularStrategy : 用第 ' + countArr + ' 个「' + arr[countArr] + '」' + '来调用接口，更新容器C');
      updateTheContainer(c, arr[countArr], countC, accessTokenWillBeRemoved);
    }
  }, interval);
}

function removeFrom(arr1, arr2) {
  console.log('geo : ' + arr2.length + '个access_token暂时失效');
  for (var i = 0, len = arr2.length; i < len; i++) {
    var index = arr1.indexOf(arr2[i]);
    if (index !== -1) {
      arr1.splice(index, 1);
    }
  }
}

module.exports = setCircularStrategy;
