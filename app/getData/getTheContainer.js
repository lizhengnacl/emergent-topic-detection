// 一次性获取N个待转换的微博存于容器C中
// 容器C的数据接口
// var c = [{
//   wid : wid,
//   coordinate : coordinate,
// }, {}, ..];

var WeiboTestData = require('../db/weiboTestData');
var arr = require('../data/accessToken');

function getTheContainer() {
  return new Promise(function(resolve, reject) {
    WeiboTestData.findAll({
      where: {
        $and: [{
          geo: {
            $ne: 'null'
          }
        }, {
          province_name: ''
        }]
      },
      // key的量 * key的连续使用次数
      limit: 150 * 3
    }).then(function(projects) {
      var c = [];
      for (var i = 0, len = projects.length; i < len; i++) {
        var data = projects[i].get({
          plain: true
        });
        var geo = data.geo;
        geo = JSON.parse(geo);
        var coordinate = geo.coordinates.reverse().toString();
        var tmp = {};
        tmp.wid = data.wid;
        tmp.coordinate = coordinate;
        c.push(tmp);
      }
      console.log('#getTheContainer : 获得容器C的容量为 ' + c.length);
      resolve(c);
    });
  });
}

module.exports = getTheContainer;
// getTheContainer().then(function(c){
//   // console.log(JSON.stringify(c, null, 4));
//   console.log('ok');
//   console.log('c[0] : ' + c[0]);
//   console.log((new Date().getTime() - t1) / 1000);
// });
