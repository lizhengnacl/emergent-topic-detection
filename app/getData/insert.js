// 插入微博文本数据

var WeiboTestData = require('../db/weiboTestData');

module.exports = function(arr) {
    for (var i = 0, len = arr.length; i < len; i++) {
        // 获得单条记录
        var obj = {};
        obj.created_at = arr[i].created_at;
        obj.wid = arr[i].id;
        obj.text = arr[i].text;
        obj.source = 0;
        obj.reposts_count = arr[i].reposts_count;
        obj.comments_count = arr[i].comments_count;
        obj.attitudes_count = arr[i].attitudes_count;
        // geo
        obj.geo = JSON.stringify(arr[i].geo);
        obj.province_name = '';
        obj.city_name = '';
        obj.district_name = '';
        obj.address = '';
        // user
        obj.uid = arr[i].user.id;
        obj.location = arr[i].user.location;
        obj.followers_count = arr[i].user.followers_count;
        obj.friends_count = arr[i].user.friends_count;
        obj.statuses_count = arr[i].user.statuses_count;
        obj.verified = arr[i].user.verified;

        // 插入数据库中
        WeiboTestData.create(obj).then(function(user) {
            // console.log('success');
        }).catch(function(err) {
            // console.log('error : 数据库插入出错');
            // console.log('file position : app/getData/insert.js');
        });
    }
}
