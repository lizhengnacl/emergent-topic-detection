// 接口调用所需的坐标信息直接在容器中获取，不与数据库打交道

var request = require('request');
var arr = require('../data/accessToken');
// getTheContainer执行后得到一个promise对象
var getTheContainer = require('./getTheContainer');
var updateTheContainer = require('./updateTheContainer');
var setCircularStrategy = require('./setCircularStrategy');
// transformGeo执行后得到一个promise对象
var updateToWeiboTestData = require('./updateToWeiboTestData');

function transformGeo() {
  getTheContainer().then(function(c) {
    // c为容器C
    var countC = {
      num: 0
    };
    // 假设只要容器C中有数据，setCircularStrategy能一直执行下去
    // 通过增大key的量，可以达到这一点
    setCircularStrategy(c, arr, updateTheContainer, countC);

    var timer = setInterval(function() {
      // 只有当容器C中数据全部更新完时，才会触发下一步操作
      // 接口的调用频率只跟setCircularStrategy有关，跟timer的频率无关
      if (countC.num >= c.length - 1) {
        // console.log(JSON.stringify(c, null, 4));
        clearInterval(timer);
        // 将c更新到数据库中
        // console.log(JSON.stringify(c, null, 4));
        updateToWeiboTestData(c).then(function() {
          console.log('done');
          transformGeo();
        });
      }
    }, 1000);
  });
}

transformGeo();

// 将正确与错误输出均输出到out文件中，便于跟踪bug
// node transformGeo.js &>> out
