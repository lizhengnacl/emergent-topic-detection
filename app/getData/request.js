var request = require('request');
var insert = require('./insert');
var arr = require('../data/accessToken');
var url = 'https://api.weibo.com/2/statuses/public_timeline.json?count=200&access_token=';

// count为arr中元素的序号
var count = 0;
var start = new Date().getTime();
var tmpUrl = url + arr[count];

// 参数设置
var interval = 5000;
var orderTime = 5 * interval;
// 如果一个access_token失效，剔除后应该立马进入下一个interval
var removeAccessToken = false;
// 存储待删除的access_token
var accessTokenWillBeRemoved = [];


var timer = setInterval(function() {

    if (arr.length === 0) {
        clearInterval(timer);
    }

    // 切换后access_token后不再用相同access_token进行请求
    if (!removeAccessToken) {
        inOrderChangeAccessToken(tmpUrl);
    }

    var end = new Date().getTime();
    if (end - start > orderTime || removeAccessToken) {
        // 重置
        removeAccessToken = false;

        if (count < arr.length - 1) {
            count += 1;
        } else {
            // 将无效的access_token剔除
            removeFrom(arr, accessTokenWillBeRemoved);
            // 清空accessTokenWillBeRemoved
            accessTokenWillBeRemoved = [];
            count = 0;
        }

        tmpUrl = url + arr[count];
        console.log('change to the ' + count + ' access token');
        inOrderChangeAccessToken(tmpUrl);
        start = new Date().getTime();
    }

}, interval);

var Request = function(url) {
    return new Promise(function(resolve, reject) {
        request.get(url, function(err, res, body) {
            if (!err && res.statusCode == 200) {
                try {
                    body = JSON.parse(body);
                } catch (e) {
                    console.log(e);
                    console.log(new Date());
                }
                // statuses为数组,数组中的元素为包含所有信息的对象
                var arr = body.statuses;
                // 将arr传递给insert函数
                resolve(arr);
            } else {
                // 将error的access_token去除掉
                addWillBeRemovedAccessTokenToArr(url);
                reject('error : 微博api返回数据有误');
            }
        });
    });
}

function inOrderChangeAccessToken(tmpUrl) {
    Request(tmpUrl).then(insert)
        .catch(function(err) {
            // console.log(err);
        });
}

function addWillBeRemovedAccessTokenToArr(url) {
    var accessToken = url.slice(url.lastIndexOf('=') + 1);
    var index = arr.indexOf(accessToken);
    if (index !== -1) {
        accessTokenWillBeRemoved.push(accessToken);
        console.log('will delete the ' + accessToken);
        // 切换到下一个access_token
        removeAccessToken = true;
    }
}

function removeFrom(arr1, arr2) {
    console.log(arr2.length + '个access_token暂时失效');
    for (var i = 0, len = arr2.length; i < len; i++) {
        var index = arr1.indexOf(arr2[i]);
        if (index !== -1) {
            arr1.splice(index, 1);
        }
    }
}
