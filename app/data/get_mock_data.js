const WeiboTestData = require('../db/weiboTestData');

WeiboTestData
    .findAll({
        where: {
            text: {
                $like: '@%'
            }
        },
        limit: 1
    })
    .then(function(result) {
        console.log(JSON.stringify(result, null, 4));
    });
