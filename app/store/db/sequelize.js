var Sequelize = require('sequelize');

var sequelize = new Sequelize('weiboTest', 'root', '8888888888', {
    host: 'localhost',
    dialect: 'mysql',
    logging: false,
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
    define: {
        charset: 'utf8mb4',
        collate: 'utf8mb4_general_ci',
    }
});

module.exports = sequelize;

// logs
// 20160309 新增表默认字符集
// show full columns from table_name;查看表中列采用的字符集
