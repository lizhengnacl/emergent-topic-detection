var Sequelize = require('sequelize');
var sequelize = require('./sequelize');

var segmentResult = sequelize.define('segmentResult', {
    id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    created_at: Sequelize.STRING,
    wid: {
        type: Sequelize.BIGINT
            // unique: true
    },
    text: Sequelize.STRING,
    // source可以考虑删除
    source: Sequelize.STRING(100),
    reposts_count: Sequelize.INTEGER,
    comments_count: Sequelize.INTEGER,
    attitudes_count: Sequelize.INTEGER,
    geo: Sequelize.STRING(90),
    province_name: Sequelize.STRING(30),
    city_name: Sequelize.STRING(30),
    district_name: Sequelize.STRING(30),
    address: Sequelize.STRING(90),
    uid: Sequelize.BIGINT,
    location: Sequelize.STRING(20),
    followers_count: Sequelize.INTEGER,
    friends_count: Sequelize.INTEGER,
    statuses_count: Sequelize.INTEGER,
    verified: Sequelize.INTEGER
}, {
    freezeTableName: true,
    timestamps: false
});

// segmentResult.sync({
//     force: true
// });

module.exports = segmentResult;
