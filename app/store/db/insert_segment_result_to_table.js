// 插入微博文本数据

var segmentResult = require('../db/segment_result_table.js');
/**
 * 存储分词后的微博数据
 * @param  {Array} arr 分词后的微博数据
 * @return {[Boolean]}
 */
module.exports = function(arr) {
    for (var i = 0, len = arr.length; i < len; i++) {
        var obj = {};
        obj.created_at = arr[i].created_at;
        obj.wid = arr[i].wid;
        obj.text = arr[i].text;
        obj.source = 0;
        obj.reposts_count = arr[i].reposts_count;
        obj.comments_count = arr[i].comments_count;
        obj.attitudes_count = arr[i].attitudes_count;
        obj.geo = arr[i].geo;
        obj.province_name = arr[i].province_name;
        obj.city_name = arr[i].city_name;
        obj.district_name = arr[i].district_name;
        obj.address = arr[i].address;
        obj.uid = arr[i].uid;
        obj.location = arr[i].location;
        obj.followers_count = arr[i].followers_count;
        obj.friends_count = arr[i].friends_count;
        obj.statuses_count = arr[i].statuses_count;
        obj.verified = arr[i].verified;
        // 插入数据库中
        segmentResult.create(obj).then(function(weibo) {
            // console.log('success');
        }).catch(function(err) {
            console.log('error : 数据库插入出错');
            console.log('file position : /home/liz/Gitlab/weiboTest/app/store/db/insert_segment_result_to_table.js');
        });
    }
}
