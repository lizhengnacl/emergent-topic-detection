/**
 * 计算词语基础权重
 * @param  {Number} frequency      词频
 * @param  {Number} maxFrequency   最高词频
 * @param  {Number} positionWeight 地理位置信息权重
 * @return {Number}                词语权重
 */
module.exports = function(frequency, maxFrequency, positionWeight) {
    return +(((frequency / maxFrequency) * 0.5 + 0.5) * positionWeight).toFixed(3)
};
