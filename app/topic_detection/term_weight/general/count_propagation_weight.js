/**
 * 计算微博文本传播特征
 * @param  {Object} obj sequelize从MySQL中获取的单个微博文本数据对象
 * @return {Number}     传播特征权值
 */
module.exports = function(obj) {
    var parameters = {
        x: 3,
        y: 2,
        z: 1
    };
    return obj.reposts_count * parameters.x + obj.comments_count * parameters.y + obj.attitudes_count * parameters.z;
};
