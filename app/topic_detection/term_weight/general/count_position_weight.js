/**
 * 计算微博的地理位置信息权重
 * @param  {Object} obj sequelize从MySQL中获取的单个微博文本数据对象
 * @return {Number}     位置权重
 */

module.exports = function(obj) {
    var str = obj.text;
    // obj.district_name.length 与 obj.district_name.length > 0 等价
    if (obj.district_name.length && str.indexOf(obj.district_name) !== -1) {
        return 1.3
    }
    if (obj.city_name.length && str.indexOf(obj.city_name) !== -1) {
        return 1.2
    }
    if (obj.province_name.length && str.indexOf(obj.province_name) !== -1) {
        return 1.1
    }
    return 1;
};
