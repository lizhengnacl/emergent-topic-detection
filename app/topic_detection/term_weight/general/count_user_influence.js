/**
 * 计算用户影响力
 * @param  {Object} obj sequelize从MySQL中获取的单个微博文本数据对象
 * @return {Number}     用户影响力权值
 */
var parameters = {
    x: 0.2,
    criticalValue: 500000,
    normal: 0.5,
    active: 0.2
};

module.exports = function(obj) {
    var activity = parameters.normal;
    if (obj.statuses_count > parameters.criticalValue) {
        activity = parameters.active;
    }
    return obj.followers_count * obj.friends_count * (1 + parameters.x * obj.verified) * activity;
};
