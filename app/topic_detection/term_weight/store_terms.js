var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var termWeightSchema = require('./mongodb/term_weight_schema.js');

mongoose.connect('localhost', 'weiboTest');

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {

});

var TermWeight = mongoose.model('TermWeight', termWeightSchema);

/**
 * 将输入的对象存储到mongodb中
 * @param  {Object} obj [description]
 * @return {Promise}     返回一个表示存储成功与否的promise对象
 */
module.exports = function(obj) {
    return new Promise(function(resolve, reject) {
        new TermWeight(obj).save(function(err) {
            if (err) reject(err);
            resolve('save success');
        });
    });
};
