/**
 * 将获取的分词结果转换为文档的形式存储到mongodb中
 * @param  {Array} arr 从segmentResult中获取的分词结果数组
 * @return {[type]}     [description]
 */
var getNWeibo = require('./get_N_weibo.js');
var mysqlToMongoDB = require('./mysql_to_mongodb.js');
var storeTerms = require('./store_terms.js');

module.exports = function(N) {
    return new Promise(function(resolve, reject) {
        getNWeibo(N).then(function(arr) {
            for (var i = 0; i < arr.length; i++) {
                var singleWeibo = mysqlToMongoDB(arr[i]);
                storeTerms(singleWeibo);
            };
            resolve('trans success');
        });
    });
};
