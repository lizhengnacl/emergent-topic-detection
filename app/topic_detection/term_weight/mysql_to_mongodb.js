/**
 * 将sequelize中MySQL中获取的微博数据对象，转为mongodb需要的document
 * @param  {Object} obj [description]
 * @return {Object}     [description]
 */
var stringToTerms = require('./string_to_terms.js');
var countSingleTermWeight = require('./general/count_single_term_weight.js');
var countPositionWeight = require('./general/count_position_weight.js');
var countPropagationWeight = require('./general/count_propagation_weight.js');
var countUserInfluence = require('./general/count_user_influence.js');

module.exports = function(obj) {
    var singleWeibo = {};
    singleWeibo.wid = obj.wid;

    // 计算用户影响力
    var userInfluence = countUserInfluence(obj);
    singleWeibo.userInfluence = userInfluence;

    // 计算位置权重
    var positionWeight = countPositionWeight(obj);

    // 计算传播特征权值
    var propagationWeight = countPropagationWeight(obj);
    singleWeibo.propagationWeight = propagationWeight;

    singleWeibo.createAt = new Date(obj.created_at).getTime();

    var stringToTermsArr = stringToTerms(obj.text);
    singleWeibo.terms = stringToTermsArr;

    singleWeibo.maxFrequency = stringToTermsArr.maxFrequency;
    // 转为hash，提高查找效率
    for (var i = 0; i < singleWeibo.terms.length; i++) {
        singleWeibo.terms[i].weight = countSingleTermWeight(singleWeibo.terms[i].frequency, singleWeibo.terms.maxFrequency, positionWeight);
    };

    singleWeibo.flag = false;
    return singleWeibo;
};
