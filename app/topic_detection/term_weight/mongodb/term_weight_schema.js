var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var termWeightSchema = new Schema({
    wid: {
        type: Number,
        index: {
            unique: true
        }
    },
    createAt: Number,
    terms: [{
        term: String,
        frequency: Number,
        weight: Number
    }],
    maxFrequency: Number,
    propagationWeight: Number,
    userInfluence: Number,
    flag: Boolean
}, {
    strict: false
});

module.exports = termWeightSchema;
