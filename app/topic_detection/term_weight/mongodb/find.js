/**
 * 按单个词进行查找
 * @return {Object} 格式{term : '词语'}
 */
var mongoose = require('mongoose');

try {
    mongoose.connect('localhost', 'weiboTest');

    var db = mongoose.connection;

    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {

    });
} catch (e) {
    console.log(e);
}

var termWeightSchema = require('./term_weight_schema.js');

var termWeight = mongoose.model('TermWeight', termWeightSchema);

module.exports = function(obj) {
    return new Promise(function(resolve, reject) {
        termWeight.findOne(obj, function(err, term) {
            if (err) console.log(err);
            resolve(term);
        })
    });
};
