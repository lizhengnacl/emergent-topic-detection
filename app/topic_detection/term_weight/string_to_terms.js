/**
 * 处理分词后的字符串
 * @param  {String} str 分词后的字符串
 * @return {Array}     [{term: '哈尔滨', frequency : 1}, {term : '天气', frequency : 1}]
 */
module.exports = function(str) {
    var reg = /[^#]+/g;
    var arr = [];
    var maxFrequency = 0;
    var tmpArr;
    while ((tmpArr = reg.exec(str)) !== null) {
        var tmp = {};
        tmp.term = tmpArr[0].slice(0, tmpArr[0].indexOf('/'))
        tmp.frequency = +tmpArr[0].charAt(tmpArr[0].length - 1);
        if (maxFrequency < tmp.frequency) {
            maxFrequency = tmp.frequency;
        }
        arr.push(tmp);
    }
    arr.maxFrequency = maxFrequency;
    return arr;
};
