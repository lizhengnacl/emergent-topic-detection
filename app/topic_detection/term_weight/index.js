var translateToDocumentAndStore = require('./translate_to_document_and_store.js');
var EventEmitter = require('events');

var myEmitter = new EventEmitter();

myEmitter.on('event', () => {
    translateToDocumentAndStore(1000).then(function(msg) {
        setTimeout(function() {
            myEmitter.emit('event');
        }, 2000);
    });
});

myEmitter.emit('event');
