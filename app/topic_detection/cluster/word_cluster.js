/**
 * 对突发词进行聚类
 * @param  {Array} arr 包含突发词的词语
 * @return {[type]}     [description]
 */
var countTwoClusterDistance = require('./general/count_two_cluster_distance.js');

module.exports = function(arr) {
    var μ = 5000;
    return new Promise(function(resolve, reject) {
        function mergeCluster() {
            // 保存待合并簇的序号
            var minDistance = 10000000000000000;
            var m = 0,
                n = 0;
            // 获取距离最短的两个簇
            for (var i = 0; i < arr.length; i++) {
                for (var j = i + 1; j < arr.length; j++) {
                    var w1 = arr[i];
                    var w2 = arr[j];
                    try {
                        var clusterDistance = countTwoClusterDistance(w1, w2);
                    } catch (e) {
                        console.log(e);
                    }
                    // console.log('计算clusterDistance距离 ' + clusterDistance);
                    if (clusterDistance < minDistance) {
                        // console.log('找到两个距离小的 ' + arr[i] + ' - ' + arr[j]);
                        minDistance = clusterDistance;
                        m = i;
                        n = j;
                    }
                };
            };

            // 合并簇
            arr[m] = arr[m] + '#' + arr[n];
            arr[n] = '#';
            console.log('合并一个词' + arr[m]);
            // 重新计算簇间最短距离
            // 由于 获取距离最短的两个簇 已经计算过簇间最短距离，
            // 现只需要在此基础上计算合并后的簇与其它簇之间的距离
            for (var k = 0; k < arr.length; k++) {
                var w1 = arr[m];
                var w2 = arr[k];
                var mergeResultDistance = countTwoClusterDistance(w1, w2);
                if (mergeResultDistance < minDistance) {
                    minDistance = mergeResultDistance;
                }
            };

            if (minDistance < μ) {
                setTimeout(function() {
                    mergeCluster();
                }, 100);
            } else {
                // 返回聚类后的结果
                resolve(arr);
            }
        }
        mergeCluster();
    });
};
