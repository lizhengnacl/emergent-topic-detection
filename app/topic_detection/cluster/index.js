// 拿到所有的词
var getAllWords = require('./mongodb/get_all_words.js');

// 聚类
var wordCluster = require('./word_cluster.js');

var emitter = require('./wait_to_load_two_term_hash_emitter.js');
emitter.on('hashOK', () => {
    getAllWords().then(function(words) {
        // 拿到所有词
        console.log('突发词集个数' + words.length);
        wordCluster(words).then(function(arr) {
            console.log(JSON.stringify(arr, null, 4));
        });
    });
})
