/**
 * 计算两个簇之间的距离
 * @param  {String} w1 '哈尔滨#天气' 或者 '#'表示已经与其它簇合并
 * @param  {String} w2 '武汉#鸭脖'
 * @return {[type]}    [description]
 */
var infinityDistance = 10000000000000000;
var countTwoWordDistance = require('./count_two_words_distance.js');
module.exports = function(w1, w2) {
    if (w1 === '#' || w2 === '#') {
        return infinityDistance;
    }

    if (w1.indexOf('#') !== -1) {
        // 表示合并的簇 '武汉#鸭脖'
        var arr1 = w1.split('#');
        var M = arr1.length;
    } else {
        // 表示单个簇 '哈尔滨'
        var arr1 = [w1];
        var M = arr1.length;
    }

    if (w2.indexOf('#') !== -1) {
        var arr2 = w2.split('#');
        var N = arr2.length;
    } else {
        var arr2 = [w2];
        var N = arr2.length;
    }

    var distanceTotal = 0;
    for (var i = 0; i < arr1.length; i++) {
        for (var j = 0; j < arr2.length; j++) {
            distanceTotal += countTwoWordDistance(arr1[i], arr2[j])
        };
    };
    return distanceTotal;
};
