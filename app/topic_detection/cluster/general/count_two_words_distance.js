/**
 * 计算两个词之间的距离
 * @param  {String} w1 '哈尔滨'
 * @param  {String} w2 '武汉'
 * @return {[type]}    [description]
 */
var infinityDistance = 10000000000000000;

// 总微博数
var total = 361097;

var emitter = require('../wait_to_load_two_term_hash_emitter.js');

// 加载hash
var getAllTwoTerm = require('../mongodb/get_all_twoterm.js');

getAllTwoTerm().then(function(twoTermObject) {
    hash = twoTermObject;
    emitter.emit('hashOK');
});

var hash = null;
module.exports = function(w1, w2) {
    if (hash !== null) {
        var str1 = w1 + '#' + w2;
        var str2 = w2 + '#' + w1;
        if (hash[str1] !== undefined) {
            return +(total / hash[str1]).toFixed(3);
        };
        if (hash[str2] !== undefined) {
            return +(total / hash[str2]).toFixed(3);
        };
        return infinityDistance;
    }
};
