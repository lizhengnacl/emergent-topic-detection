var mongoose = require('mongoose');

var connect = require('./connect.js');
connect();

var burstyTermSchema = require('./bursty_term.js');

var BurstyTerm = mongoose.model('burstyTerm', burstyTermSchema);

/**
 * 从突发词集中获取N个突发词，仅仅只有词语
 * @param  {Number} N
 * @param  {Number} offset 偏移量
 * @return {Array}        Promise对象，包含N个突发词数组[{_id : '1', term : '哈哈'}, {}], {}
 */
module.exports = function(N, offset) {
    return new Promise(function(resolve, reject) {
        BurstyTerm.find().skip(offset).limit(N).select({
            term: 1
        }).exec(function(err, words) {
            resolve(words)
        });
    });
};
