/**
 * 拿到突发词集中的所有词，只需要词
 * @return {[Promise]} Promise对象，包含突发词集[{_id : 1, twoTerm : '武汉#周黑鸭', commonCounts : 1}, {}]
 * 注意：
 * 越到后越少
 * 取出的是commonCounts > 0的
 */
var getNtwoTerm = require('./get_N_twoterm.js');

module.exports = function() {
    var twoTermObject = {};
    var N = 10000;
    var offset = 0;
    return new Promise(function(resolve, reject) {
        function cycleN() {
            getNtwoTerm(N, offset).then(function(twoTerms) {
                for (var i = 0; i < twoTerms.length; i++) {
                    if (twoTermObject[twoTerms[i].twoTerm] === undefined) {
                        twoTermObject[twoTerms[i].twoTerm] = twoTerms[i].commonCounts;
                    };
                };
                offset += N;
                if (offset < 30239) {
                    cycleN();
                    console.log('get N two term');
                } else {
                    resolve(twoTermObject);
                }
            });
        }
        cycleN();
    });
};
