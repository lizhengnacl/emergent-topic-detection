/**
 * 拿到突发词集中的所有词，只需要词
 * @return {[Promise]} Promise对象，包含突发词集['哈哈', '', '', '']
 */
var getNBurstyWords = require('./get_N_bursty_words.js');

module.exports = function() {
    var burstyWordsArr = [];
    var N = 10;
    var offset = 0;
    return new Promise(function(resolve, reject) {
        function cycleN() {
            getNBurstyWords(N, offset).then(function(words) {
                for (var i = 0; i < words.length; i++) {
                    if (burstyWordsArr.indexOf(words[i].term) === -1) {
                        burstyWordsArr.push(words[i].term)
                    }
                };
                offset += N;
                if (offset < 3318) {
                    cycleN();
                    console.log('get N words');
                } else {
                    resolve(burstyWordsArr)
                }
            });
        }
        cycleN();
    });
};
