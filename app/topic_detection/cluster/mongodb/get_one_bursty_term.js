var mongoose = require('mongoose');

var connect = require('./connect.js');
connect();

var burstyTermSchema = require('./bursty_term.js');

var burstyTermSchema = mongoose.model('burstyTerm', burstyTermSchema);

/**
 * 从突发词集中获取N个突发词
 * @param  {Number} N
 * @param  {Number} offset 偏移量
 * @return {Array}        Promise对象，包含N个突发词数组
 */
module.exports = function() {
    return new Promise(function(resolve, reject) {
        burstyTermSchema.findOne({
            flag: false
        }, function(err, term) {
            resolve(term);
        })
    });
};
