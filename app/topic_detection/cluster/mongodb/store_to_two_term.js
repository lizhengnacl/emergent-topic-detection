var getOneBurstyTerm = require('./get_one_bursty_term.js');

var storeNToTwoTerm = require('./store_N_two_term.js');

var EventEmitter = require('events');

var myEmitter = new EventEmitter();

module.exports = function() {
    var burstyTermTotal = 3318;
    var N = 20;
    var offset = 0;

    myEmitter.on('getN', (term) => {
        console.log('getN');
        storeNToTwoTerm(N, offset, term).then(function(msg) {
            console.log(msg);
            offset += N;
            if (offset < burstyTermTotal) {
                myEmitter.emit('getN', term);
            } else {
                myEmitter.emit('startNew');
            }
        });
    });

    myEmitter.on('startNew', () => {
        offset = 0;
        console.log('startNew');
        getOneBurstyTerm().then(function(term) {
            if (term === null) {
                myEmitter.removeListener('startNew', function() {
                    console.log('bursty term to two term ok');
                });
            };
            myEmitter.emit('getN', term);
            term.flag = true;
            term.save(function() {
                console.log('update one');
            });
        });
    });

    myEmitter.emit('startNew');
};
