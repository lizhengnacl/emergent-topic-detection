/**
 * 将一个词与N个词的匹配结果存入two term中
 * @param  {Number} N      选出N个词
 * @param  {Number} offset 偏移量
 * @param  {Object} term   传入的一个词，可用.save()直接更新
 * @return {Promise}
 */
var getNBurstyTerm = require('./get_N_bursty_term.js');
var countCommonCounts = require('../general/count_common_counts.js');

var mongoose = require('mongoose');
var twoTermSchema = require('./two_term.js');
var TwoTerm = mongoose.model('twoTerm', twoTermSchema);

module.exports = function(N, offset, term) {
    return new Promise(function(resolve, reject) {
        getNBurstyTerm(N, offset).then(function(terms) {
            var count = 0;
            var totalCount = terms.length - 1;
            var timerStart = new Date().getTime();

            // 获取的N个突发词中，第一个与传入进来的term相同，自己不用跟自己判断相似度
            for (var i = 1; i < terms.length; i++) {
                var obj = {};
                obj.twoTerm = term.term + '#' + terms[i].term;
                obj.commonCounts = countCommonCounts(term.wids, terms[i].wids);
                new TwoTerm(obj).save(function() {
                    // console.log('save success');
                    count++;
                });
            };

            var timer = setInterval(function() {
                if (count === totalCount) {
                    resolve('save to two term ok')
                    clearInterval(timer);
                }
                if (new Date().getTime() - timerStart > 5000) {
                    resolve('save to two term time out')
                    clearInterval(timer);
                }
            }, 100);
        });
    });
};
