var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var twoTermSchema = new Schema({
    twoTerm: {
        type: String,
        index: true
    },
    commonCounts: Number,
    flag: {
        type: Boolean,
        default: false
    }
});

module.exports = twoTermSchema;
