var mongoose = require('mongoose');

// var connect = require('./connect.js');
// connect();

var burstyTermSchema = require('./bursty_term.js');

var burstyTermSchema = mongoose.model('burstyTerm', burstyTermSchema);

/**
 * 从突发词集中获取N个突发词，包含所有信息
 * @param  {Number} N
 * @param  {Number} offset 偏移量
 * @return {Array}        Promise对象，包含N个突发词数组
 */
module.exports = function(N, offset) {
    return new Promise(function(resolve, reject) {
        burstyTermSchema.find({
            flag: false
        }).skip(offset).limit(N).exec(function(err, terms) {
            resolve(terms)
        });
    });
};
