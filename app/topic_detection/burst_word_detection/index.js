var handleNWeibo = require('./handle_N_weibo.js');
var EventEmitter = require('events');

var myEmitter = new EventEmitter();

myEmitter.on('event', () => {
    handleNWeibo(1).then(function(msg) {
        console.log(msg);
        // setTimeout(function() {
        myEmitter.emit('event');
        // }, 100);
    });
});

myEmitter.emit('event');
