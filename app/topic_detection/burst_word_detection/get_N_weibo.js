/**
 * 从termWeight中获取N个微博
 * @param  {Number} N
 * @return {Array}   含有N个微博的数据
 */
var mongoose = require('mongoose');

try {
    mongoose.connect('localhost', 'weiboTest');

    var db = mongoose.connection;

    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {

    });
} catch (e) {
    console.log(e);
}

var termWeightSchema = require('../term_weight/mongodb/term_weight_schema.js');

var TermWeight = mongoose.model('TermWeight', termWeightSchema);

module.exports = function(N) {
    return new Promise(function(resolve, reject) {
        TermWeight.findOne({
            flag: false
        }, function(err, oneWeibo) {
            // console.log(JSON.stringify(oneWeibo, null, 4));
            // console.log(oneWeibo['家']);
            // console.log(oneWeibo['maxFrequency']);
            // 提交一个问题试试
            resolve(oneWeibo);
        });
    });
};
