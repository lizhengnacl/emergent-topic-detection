/**
 * 处理N条微博
 * @param  {Number} N 一次只处理N条微博
 * @return {[type]}   [description]
 * singleTerm表示从singleTermOneTimeWindown表中取出的一个document
 * oneWeibo表示从termWeight表中取出的一个document
 */
var getNWeibo = require('./get_N_weibo.js');
// 在SingleTermOneTimeWindown中查找
var find = require('./mongodb/find_single_term_weight_within_one_time_window.js');
// 在SingleTermOneTimeWindown中查找
var create = require('./mongodb/create_single_term_weight_within_one_time_window.js');

module.exports = function(N) {
    return new Promise(function(resolve, reject) {
        // 在termWeight中查找一条微博数据，包含多个词语
        getNWeibo(N).then(function(oneWeibo) {
            var count = 0;
            var reachCount = oneWeibo.terms.length;
            var time = 0;
            var reachTime = 1000;
            for (var i = 0; i < oneWeibo.terms.length; i++) {
                var obj = {
                    term: oneWeibo.terms[i].term
                };
                // 在SingleTermOneTimeWindown中查找词语
                (function(i) {
                    find(obj).then(function(singleTerm) {
                        if (singleTerm !== null) {
                            singleTerm.singleTermTotalWeight += oneWeibo.terms[i].weight * oneWeibo.propagationWeight;
                            var singleTermWid = {};
                            singleTermWid.wid = oneWeibo.wid;
                            singleTermWid.userInfluence = oneWeibo.userInfluence;
                            singleTerm.wids.push(singleTermWid);
                            singleTerm.save(function(err) {
                                if (err) console.log(err);
                                count++;
                            });
                        } else {
                            var term = oneWeibo.terms[i].term;
                            var singleTerm = {
                                term: oneWeibo.terms[i].term,
                                wids: [{
                                    wid: oneWeibo.wid,
                                    userInfluence: oneWeibo.userInfluence
                                }],
                                singleTermTotalWeight: oneWeibo.terms[i].weight * oneWeibo.propagationWeight
                            };
                            create(singleTerm).then(function(singleTerm) {
                                console.log('create');
                                count++;
                            });
                        }
                    });
                })(i);
            };
            // 标记已经处理过的微博
            oneWeibo.flag = true;
            oneWeibo.save();
            // 等待时间过长时，直接跳过
            var timer = setInterval(function() {
                time += 100;
                if (count === reachCount) {
                    resolve('handle 1 weibo ok');
                    clearInterval(timer);
                }
                if (time > reachTime) {
                    resolve('jump handle 1 weibo ok');
                    clearInterval(timer);
                }
            }, 100);
        });
    });
};
