var mongoose = require('mongoose');

var singleTermOneTimeWindownSchema = require('./single_term_weight_within_one_time_window.js');

var connect = require('./connect.js');
connect();

var SingleTermOneTimeWindown = mongoose.model('SingleTermOneTimeWindown', singleTermOneTimeWindownSchema);

/**
 * 将输入的对象存储到singleTermOneTimeWindown中
 * @param  {Object} obj [description]
 * @return {Promise}     返回一个表示存储成功与否的promise对象，该对象包含输入对象的全部信息
 */
module.exports = function(obj) {
    return new Promise(function(resolve, reject) {
        new SingleTermOneTimeWindown(obj).save(function(err, obj) {
            if (err) reject(err);
            resolve(obj);
        });
    });
};
