var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var singleTermOneTimeWindownSchema = new Schema({
    term: {
        type: String,
        index: {
            unique: true
        }
    },
    wids: [{
        wid: Number,
        userInfluence: Number
    }],
    singleTermTotalWeight: Number
}, {
    strict: false
});

module.exports = singleTermOneTimeWindownSchema;
