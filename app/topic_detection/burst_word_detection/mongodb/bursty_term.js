var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var burstyTerm = new Schema({
    term: {
        type: String,
        index: {
            unique: true
        }
    },
    wids: [{
        wid: Number,
        userInfluence: Number
    }],
    singleTermTotalWeight: Number,
    flag: false
}, {
    strict: false
});

module.exports = burstyTerm;
