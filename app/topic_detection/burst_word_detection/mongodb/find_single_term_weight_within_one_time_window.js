/**
 * 在singleTermOneTimeWindown中按关键词进行查找
 * @param  {[Object]} obj 格式{term : '词语'}
 * @return {Promise}     包含一个词语对象的promise对象
 */
var mongoose = require('mongoose');
var singleTermOneTimeWindownSchema = require('./single_term_weight_within_one_time_window.js');

var connect = require('./connect.js');
connect();

var SingleTermOneTimeWindown = mongoose.model('SingleTermOneTimeWindown', singleTermOneTimeWindownSchema);

module.exports = function(obj) {
    // console.log(JSON.stringify(obj, null, 4));
    return new Promise(function(resolve, reject) {
        SingleTermOneTimeWindown.findOne(obj, function(err, term) {
            if (err) console.log(err);
            resolve(term);
        })
    });
};
