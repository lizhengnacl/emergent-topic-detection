var mongoose = require('mongoose');

module.exports = function() {
    try {
        mongoose.connect('localhost', 'weiboTest');

        var db = mongoose.connection;

        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function() {

        });
    } catch (e) {
        console.log(e);
    }
};
