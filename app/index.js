var EventEmitter = require('events');
var filterWeibo = require('./pre/filter_weibo.js');
var segmentAndFilter = require('./segment/segment_and_filter.js');
var insertSegmentResult = require('./store/db/insert_segment_result_to_table.js');
/**
 * 配置文件
 * N 一次去除的微博个数
 * statusCountLimit 允许状态数上限
 * 处理后的arr可能为空
 * @type {Object}
 */
var config = {
    N: 1000,
    statusCountLimit: 1000000
};

var myEmitter = new EventEmitter();

myEmitter.on('event', () => {
    filterWeibo(config).then(function(arr) {
        segmentAndFilter(arr);
        console.log('insertSegmentResult arr.length: ' + arr.length);
        // console.log(JSON.stringify(arr, null, 4));
        insertSegmentResult(arr);
        setTimeout(function() {
            myEmitter.emit('event');
        }, 10000);
    });
});

myEmitter.emit('event');
